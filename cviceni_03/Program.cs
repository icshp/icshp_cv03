﻿using System;
using Fei.BaseLib;

namespace cviceni_03
{
    class Program
    {
        public static object ZiskejJmeno(Student student)
        {
            return student.Jmeno;
        }

        public static object ZiskejCislo(Student student)
        {
            return student.Cislo;
        }

        public static object ZiskejFakultu(Student student)
        {
            return student.Fakulta;
        }

        public static object ZiskejStudenta(Student student)
        {
            return student;
        }

        static int Main(string[] args)
        {
            TabulkaStudentu tabStud = new TabulkaStudentu();
            Student student = new Student();
            student.Jmeno = "Studenticek";
            student.Cislo = 1;
            student.Fakulta = Fakulta.FEI;
            //Přidej
            Console.WriteLine(tabStud.VlozStudenta(ZiskejStudenta, student));
            Student student2 = new Student();
            student2.Jmeno = "Studenticek2";
            student2.Cislo = 2;
            student2.Fakulta = Fakulta.FEI;
            Console.WriteLine(tabStud.VlozStudenta(ZiskejStudenta, student2));
            Console.WriteLine("Studenti v poli:");
            tabStud.studenti.VypisStudenty();
            //Hledej
            Console.WriteLine("Hledej: \n" + tabStud.NajdiStudenta(ZiskejJmeno, "Studenticek").VypisStudenta());
            //Odeber
            Console.WriteLine("Odeber: \n" + tabStud.OdeberStudenta(ZiskejJmeno, "Studenticek"));
            tabStud.studenti.VypisStudenty();
            return 0;
           /* Studenti studenti = new Studenti();
            bool run = true;
            while (run)
            {
                Console.WriteLine("Menu:\n1)Načti studenta\n2)Výpis studenta\n3)Seřazení podle jména\n4)Seřazení podle čísla\n5)Seřazení podle fakulty\n6)Římská na decimální čísla\n7)Decimální na římská čísla\n8)Konec");
                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        studenti.NactiStudenta();
                        break;
                    case 2:
                        studenti.VypisStudenty();
                        break;
                    case 3:
                        studenti.SeraditPodleJmena();
                        break;
                    case 4:
                        studenti.SeraditPodleCisla();
                        break;
                    case 5:
                        studenti.SeraditPodleFakulty();
                        break;
                    case 6:
                        Console.WriteLine(MathConvertor.RomanToDecimal(Reading.ReadString("Římské číslo")).ToString());
                        break;
                    case 7:
                        Console.WriteLine(MathConvertor.DecimalToRoman(Reading.ReadInt("Decimální číslo")));
                        break;
                    case 8:
                        return 0;
                    default:
                        Console.WriteLine("Opakujte zadaní:");
                        break;
                }
            }
            return 0;*/
        }
    }
}
