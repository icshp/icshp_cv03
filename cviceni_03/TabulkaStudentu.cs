﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cviceni_03
{
    delegate Object ZiskejKlic(Student student);
    class TabulkaStudentu
    {
        public Studenti studenti;
        public TabulkaStudentu()
        {
            studenti = new Studenti();
        }

        public Student NajdiStudenta(ZiskejKlic ziskejKlic, object hledanaHodnota) {
            foreach (Student student in studenti.Pole)
            {
                if (ziskejKlic(student) == hledanaHodnota) {
                    return student;
                }
            }
            return null;
        }

        public bool VlozStudenta(ZiskejKlic ziskejKlic, Student student)
        {
            if (NajdiStudenta(ziskejKlic, student) == null)
            {
                studenti.PridejStudenta(student);
                return true;
            }
            else { 
                return false;
            }
        }

        public bool OdeberStudenta(ZiskejKlic ziskejKlic, object hledanaHodnota)
        {
            Student student = NajdiStudenta(ziskejKlic, hledanaHodnota);
            if (student == null)
            {
                return false;
            }
            else
            {
                studenti.OdeberStudenta(student);
                return true;
            }
        }
    }
}
