﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cviceni_03
{
    class Student
    {
        private string jmeno;
        private int cislo;
        private Fakulta fakulta;


        public string Jmeno
        {
            get { return jmeno; }
            set { jmeno = value; }
        }
        public int Cislo
        {
            get { return cislo; }
            set { cislo = value; }
        }
        public Fakulta Fakulta
        {
            get { return fakulta; }
            set { fakulta = value; }
        }

        public string VypisStudenta()
        {
            return "Jmeno:" + jmeno.ToString() + " Cislo:" + cislo.ToString() + " Fakulta:" + fakulta.ToString();
        }

    }
}
