﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cviceni_03
{
    class Studenti
    {
        private Student[] pole = new Student[0];

        public Student[] Pole
        {
            get { return pole; }
            set { pole = value; }
        }

        public void PridejStudenta(Student student)
        {
            List<Student> temp = new List<Student>(pole);
            temp.Add(student);
            pole = temp.ToArray();
        }

        public void OdeberStudenta(Student student)
        {
            List<Student> temp = new List<Student>(pole);
            temp.Remove(student);
            pole = temp.ToArray();
        }

        public void NactiStudenta()
        {
            List<Student> temp = new List<Student>(pole);
            Student novyStudent = new Student();
            Console.WriteLine("Zadejte prosím jméno studenta:");
            novyStudent.Jmeno = Console.ReadLine().ToString();
            Console.WriteLine("Zadejte prosím číslo:");
            novyStudent.Cislo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Zvolte prosím fakultu:\n1)FES\n2)FF\n3)FEI\n4)FCHT");
            bool run = true;
            while (run)
            {
                switch (Console.ReadLine())
                {
                    case "FES":
                        novyStudent.Fakulta = Fakulta.FES;
                        run = false;
                        break;
                    case "FF":
                        novyStudent.Fakulta = Fakulta.FF;
                        run = false;
                        break;
                    case "FEI":
                        novyStudent.Fakulta = Fakulta.FEI;
                        run = false;
                        break;
                    case "FCHT":
                        novyStudent.Fakulta = Fakulta.FCHT;
                        run = false;
                        break;
                    default:
                        Console.WriteLine("Chybné zadání. Opakujte prosím.");
                        break;
                }
            }
            temp.Add(novyStudent);
            pole = temp.ToArray();
        }

        public void VypisStudenty()
        {
            foreach (Student student in pole)
            {
                Console.WriteLine(student.VypisStudenta());
            }
        }

        public void SeraditPodleJmena()
        {
            Array.Sort(pole, delegate (Student student1, Student student2) {
                return student1.Jmeno.CompareTo(student2.Jmeno);
            });
            VypisStudenty();
        }

        public void SeraditPodleCisla()
        {
            Array.Sort(pole, delegate (Student student1, Student student2) {
                return student1.Cislo.CompareTo(student2.Cislo);
            });
            VypisStudenty();
        }

        public void SeraditPodleFakulty()
        {
            Array.Sort(pole, delegate (Student student1, Student student2) {
                return student1.Fakulta.CompareTo(student2.Fakulta);
            });
            VypisStudenty();
        }
    }
}
