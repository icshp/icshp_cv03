﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei
{
    namespace BaseLib
    {
        //Třída MathConvertor umožnující převody číslených soustav
        /// <summary>
        /// Obsahuje metody pro převody mezi číselnými sousatavami a také metody pro převody do/z římských čísel.
        /// </summary>
        public class MathConvertor
        {

            //Metoda pro převod mezi desítkovou a binární soustavou
            /// <summary>
            /// Načte vstupní hodnotu typu int a převede jí na binární čísla, která vrátí jako string.
            /// </summary>
            /// <returns>
            /// binární číslo
            /// </returns>
            /// <param name="value">Vstupní hodnota (Decimální)</param>
            /// <exception cref="InvalidCastException">
            /// Hodnotu nebude možné převést
            /// </exception>
            public static int DecimalToBin(int value)
            {
                return Convert.ToInt32(Convert.ToString(value, 2));
            }

            //Metoda pro převod mezi binární a desítkovou soustavou
            /// <summary>
            /// Načte vstupní hodnotu binární hodnotu a převede jí na desítkovou soustavu.
            /// </summary>
            /// <returns>
            /// Decimální číslo
            /// </returns>
            /// <param name="value">Vstupní hodnota (binarní)</param>
            /// <exception cref="OverflowException">
            /// Přetečení integeru
            /// </exception>
            public static int BinToDecimal(int value)
            {
                return Convert.ToInt32(Convert.ToString(value, 10));
            }

            //Metoda pro převod mezi desítkovou soustavou a římským číslem
            /// <summary>
            /// Načte vstupní hodnotu v desítkové soustavě a převede jí na římské číslo.
            /// </summary>
            /// <returns>
            /// Římské číslo (String) 
            /// </returns>
            /// <param name="number">Vstupní hodnota (desítková)</param>
            /// <exception cref="ArgumentOutOfRangeException">
            /// Překročení maximální velikosti římského čísla nebo nespecifikovaná chyba.
            /// </exception>
            public static string DecimalToRoman(int number)
            {
                if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("Hodnota musí být mezi 1 a 3999");
                if (number < 1) return string.Empty;
                if (number >= 1000) return "M" + DecimalToRoman(number - 1000);
                if (number >= 900) return "CM" + DecimalToRoman(number - 900);
                if (number >= 500) return "D" + DecimalToRoman(number - 500);
                if (number >= 400) return "CD" + DecimalToRoman(number - 400);
                if (number >= 100) return "C" + DecimalToRoman(number - 100);
                if (number >= 90) return "XC" + DecimalToRoman(number - 90);
                if (number >= 50) return "L" + DecimalToRoman(number - 50);
                if (number >= 40) return "XL" + DecimalToRoman(number - 40);
                if (number >= 10) return "X" + DecimalToRoman(number - 10);
                if (number >= 9) return "IX" + DecimalToRoman(number - 9);
                if (number >= 5) return "V" + DecimalToRoman(number - 5);
                if (number >= 4) return "IV" + DecimalToRoman(number - 4);
                if (number >= 1) return "I" + DecimalToRoman(number - 1);
                throw new ArgumentOutOfRangeException("Chyba.");
            }

            //Metoda pro převod mezi římským číslem a desítkovou soustavou
            /// <summary>
            /// Načte vstupní hodnotu římského čísla a převede jí na číslo v desítkové soustavě.
            /// </summary>
            /// <returns>
            /// Číslo v desítkové soustavě (Int) 
            /// </returns>
            /// <param name="input">Vstupní hodnota (Římské číslo)</param>
            public static int RomanToDecimal(string input)
            {
                int res = 0;

                for (int i = 0; i < input.Length; i++)
                {
                    //Načtení aktuálního řetězce
                    int s1 = input[i];

                    if (i + 1 < input.Length)
                    {
                        int s2 = input[i + 1];

                        if (s1 >= s2)
                        {
                            res = res + s1;
                        }
                        else
                        {
                            res = res + s2 - s1;
                            i++;
                        }
                    }
                    else
                    {
                        res = res + s1;

                    }
                }
                return res;
            }

            //Pomocná metoda převádějící římská čísla na desítková čísla
            /// <summary>
            /// Načte vstupní hodnotu (římské číslo) a vrátí číslo z desítkové soustavy
            /// </summary>
            /// <returns>
            /// Číslo v desítkové soustavě (Int) 
            /// </returns>
            /// <param name="r">Vstupní hodnota (Římské číslo)</param>
            int hodnotaCisla(char r)
            {
                if (r == 'I')
                    return 1;
                if (r == 'V')
                    return 5;
                if (r == 'X')
                    return 10;
                if (r == 'L')
                    return 50;
                if (r == 'C')
                    return 100;
                if (r == 'D')
                    return 500;
                if (r == 'M')
                    return 1000;

                return -1;
            }

        }
    }
}
