﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei
{
    namespace BaseLib
    {
        //Třída Reading umožnující převod vstupu na zvolený datový typ
        /// <summary>
        /// Contains all methods for performing basic parse functions.
        /// </summary>
        /// <remarks>
        /// <para> Tato třída může přečíst cokoliv a převét to na zvolený datový typ.</para>
        /// </remarks>
        public class Reading
        {
            //Převádí vstup na Double
            /// <summary>
            /// Načte vstup z konzole a pokusí se ho převést na Double.
            /// </summary>
            /// <returns>
            /// Double value or exception
            /// </returns>
            /// <param name="value">Vstupní string pro vypsání na obrazovku</param>
            /// <exception cref="InvalidCastException">
            /// Nepřeveditelné na Double
            /// </exception>
            public static double ReadDouble(string value)
            {
                Console.Write("{0}: ", value);
                return Convert.ToDouble(Console.ReadLine());
            }

            //Převádí vstup na Int32
            /// <summary>
            /// Načte vstup z konzole a pokusí se ho převést na Int32.
            /// </summary>
            /// <returns>
            /// Int32 value or exception
            /// </returns>
            /// <param name="value">Vstupní string pro vypsání na obrazovku</param>
            /// <exception cref="InvalidCastException">
            /// Nepřeveditelné na Int32
            /// </exception>
            public static int ReadInt(string value)
            {
                Console.Write("{0}: ", value);
                return Convert.ToInt32(Console.ReadLine());

            }

            //Převádí vstup na Char
            /// <summary>
            /// Načte vstup z konzole a pokusí se ho převést na Char.
            /// </summary>
            /// <returns>
            /// Char value or exception
            /// </returns>
            /// <param name="value">Vstupní string pro vypsání na obrazovku</param>
            /// <exception cref="InvalidCastException">
            /// Nepřeveditelné na Char
            /// </exception>
            public static char ReadChar(string value)
            {
                Console.Write("{0}: ", value);
                return Convert.ToChar(Console.ReadLine());
            }

            //Převádí vstup na String
            /// <summary>
            /// Načte vstup z konzole a pokusí se ho převést na String.
            /// </summary>
            /// <returns>
            /// String value or exception
            /// </returns>
            /// <param name="value">Vstupní string pro vypsání na obrazovku</param>
            /// <exception cref="InvalidCastException">
            /// Nepřeveditelné na String
            /// </exception>
            public static string ReadString(string value)
            {
                Console.Write("{0}: ", value);
                return Convert.ToString(Console.ReadLine());
            }
        }
    }
}
