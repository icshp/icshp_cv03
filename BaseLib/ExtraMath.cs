﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fei
{
    namespace BaseLib
    {
        //Třída ExtraMath umožnující pokročilé matematické operace
        /// <summary>
        /// Obsahuje metody pro výpočet kořenů kvadratické rovnice a generátor náhodných čísel typu double.
        /// </summary>
        public class ExtraMath
        {
            //Metoda pro výpočet kořenů kvadratické rovnice
            /// <summary>
            /// Načte vstupy a, b, c typu double a vrátí výstupní hodnotny x1 a x2.
            /// Také bool jestli je nalezeno řešení v reálných číslech.
            /// </summary>
            /// <returns>
            /// x1 a x2 -> Kořeny rovnice
            /// return -> Je řešitelná v oboru reálných čísel?
            /// </returns>
            /// <param name="a">Vstupní koeficient</param>
            /// <param name="b">Vstupní koeficient</param>
            /// <param name="c">Vstupní koeficient</param>
            /// <param name="x1">Výstupní parametr, kořen č. 1</param>
            /// <param name="x2">Výstupní parametr, kořen č. 2</param>
            /// <result cref="Infinity">
            /// Nulové hodnoty (Dělení nulou)
            /// </result>
            public static bool CalcQuadratic(double a, double b, double c, out double x1, out double x2)
            {
                double d = Math.Pow(b, 2) - 4 * a * c;

                if (d >= 0)
                {
                    if (d > 0)
                    {
                        x1 = (-b + Math.Sqrt(d)) / (2 * a);
                        x2 = (-b - Math.Sqrt(d)) / (2 * a);
                    }
                    else
                    {
                        x1 = x2 = (-b + Math.Sqrt(d)) / (2 * a);
                    }
                    return true;
                }
                else
                {
                    x1 = x2 = 0;
                    return false;
                }
            }

            //Metoda pro generování pseudonáhodného čísla v zadaném rozsahu.
            /// <summary>
            /// Metoda načte rozsah pro generování pseudonáhodného čísla a instanci třídy random.
            /// </summary>
            /// <returns>
            /// pseudonáhodné číslo typu double
            /// </returns>
            /// <param name="min">Minimální hodnota</param>
            /// <param name="max">Maximální hodnota</param>
            /// <param name="random">Instance třídy random</param>
            /// <exception cref = "ArgumentNullException" >
            /// Nedoplněné hodnoty
            /// </exception>
            public static double RandomDouble(Random random, int min, int max)
            {
                return random.NextDouble() * (max - min) + min;
            }
        }
    }
}
